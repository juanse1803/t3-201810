package model.vo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

import model.data_structures.*;

/**
 * Representa un servicio que se encuentra en el archivo JSON a leer
 * @author Geovanny Gonzalez
 */

public class Servicio implements Comparable<Servicio> {

	//=====================================
	// Atributos
	//=====================================

	/**
	 * Representa el nombre de la compa�ia del taxi.
	 */

	@SerializedName("company") // SerializedName representa la clave asociada en el JSON.	
	private String nombreCompania;	

	/**
	 * Censo de area de bajada (Clave: dropoff_census_track)
	 */

	@SerializedName("dropoff_census_track")
	private double censoAreaBajada;

	/**
	 * Latitud de la zona de la ciudad donde termino el servicio (Clave: dropoff_centroid_latitude)	 *  
	 */

	@SerializedName("dropoff_centroid_latitude")
	private double latitudZonaCiudadBajada;

	/**
	 * Zona de la ciudad donde se termino el servicio. (Clave: dropoff_centroid_location)
	 */

	@SerializedName("dropoff_centroid_location")
	private localizacionZonaCiudad localizacionBajada;

	/**
	 * Longitud de la zona de la ciudad donde termino el servicio (Clave: dropoff_centroid_longitude)
	 */

	@SerializedName("dropoff_centroid_longitude")
	private double longitudZonaCiudadBajada;

	/**
	 * Area donde finalizo el servicio (Clave: dropoff_community_area)
	 */

	@SerializedName("dropoff_community_area")
	private double areaFinalizacion;

	/**
	 * Numero de extras realizados por el taxi (Clave: extras)
	 */

	@SerializedName("extras")
	private double extras;

	/**
	 * Numero de pasajeros en el taxi (Clave: fare)
	 */

	@SerializedName("fare")
	private double pasajeros;

	/**
	 * Forma de pago para el servicio (Clave: payment_type)
	 */

	@SerializedName("payment_type")
	private String formaPago;

	//	
	/**
	 * Censo de area de recogida (Clave: pickup_census_track)
	 */

	@SerializedName("pickup_census_track")
	private double censoAreaRecogida;

	/**
	 * Latitud de la zona de la ciudad donde comenzo el servicio (Clave: pickup_centroid_latitude)  
	 */

	@SerializedName("pickup_centroid_latitude")
	private double latitudZonaCiudadRecogida;

	/**
	 * Zona de la ciudad donde se comenzo el servicio. (Clave: pickup_centroid_location)
	 */

	@SerializedName("pickup_centroid_location")
	private localizacionZonaCiudad localizacionRecogida;

	/**
	 * Longitud de la zona de la ciudad donde comenzo el servicio (Clave: pickup_centroid_longitude)
	 */

	@SerializedName("pickup_centroid_longitude")
	private double longitudZonaCiudadRecogida;

	/**
	 * Area donde comenzo el servicio (Clave: pickup_community_area)
	 */

	@SerializedName("pickup_community_area")
	private double areaComienzo;	
	//

	/**
	 * Identificacion del taxi. (Clave: taxi_id)
	 */

	@SerializedName("taxi_id")
	private String identificacionTaxi;

	/**
	 * Numero de dolares en propina (Clave: tips)
	 */

	@SerializedName("tips")
	private double propina;

	/**
	 * Numero de recargos, o cobro extra por recargos (Clave: tolls)
	 */

	@SerializedName("tolls")
	private double recargos;

	/**
	 * Cadena de texto con la fecha y hora de la finalizacion del recorrido (Clave: trip_end_timestamp)
	 */

	@SerializedName("trip_end_timestamp")
	private String fechaHoraFinal;

	/**
	 * Identificacion del servicio. (Clave: trip_id)
	 */

	@SerializedName("trip_id")
	private String identificacionServicio;	

	/**
	 * Numero de millas recorridas. (Clave: trip_miles)
	 */

	@SerializedName("trip_miles")
	private double millasRecorridas;

	/**
	 * Tiempo en segundos recorridos. (Clave: trip_seconds)
	 */

	@SerializedName("trip_seconds")
	private double segundosEmpleados;

	/**
	 * Cadena de texto con la fecha y hora del inicio del recorrido (Clave: trip_start_timestamp)
	 */

	@SerializedName("trip_start_timestamp")
	private String fechaHoraInicial;

	/**
	 * Valor total del servicio. (Clave: trip_total)
	 */

	@SerializedName("trip_total")
	private double totalServicio;

	//=====================================
	// Constructor
	//=====================================

	public Servicio(String nombreCompania, String censoAreaBajada, String latitudZonaCiudadBajada,
			localizacionZonaCiudad localizacionBajada, String longitudZonaCiudadBajada, String areaFinalizacion,
			String extras, String pasajeros, String formaPago, String censoAreaRecogida, String latitudZonaCiudadRecogida,
			localizacionZonaCiudad localizacionRecogida, String longitudZonaCiudadRecogida, String areaComienzo, String identificacionTaxi,
			String propina, String recargos, String fechaHoraFinal, String identificacionServicio, String millasRecorridas, String segundosEmpleados,
			String fechaHoraInicial, String totalServicio)
	{		
		try
		{
			this.nombreCompania = nombreCompania;
			this.censoAreaBajada = Double.parseDouble(censoAreaBajada);
			this.latitudZonaCiudadBajada = Double.parseDouble(latitudZonaCiudadBajada);
			this.localizacionBajada = localizacionBajada;
			this.longitudZonaCiudadBajada = Double.parseDouble(longitudZonaCiudadBajada);
			this.areaFinalizacion = Double.parseDouble(areaFinalizacion);
			this.extras = Double.parseDouble(extras);
			this.pasajeros = Double.parseDouble(pasajeros);
			this.formaPago = formaPago;
			this.censoAreaRecogida = Double.parseDouble(censoAreaRecogida);
			this.latitudZonaCiudadRecogida = Double.parseDouble(latitudZonaCiudadRecogida);
			this.localizacionRecogida = localizacionRecogida;
			this.longitudZonaCiudadRecogida = Double.parseDouble(longitudZonaCiudadRecogida);
			this.areaComienzo = Double.parseDouble(areaComienzo);
			this.identificacionTaxi = identificacionTaxi;
			this.propina = Double.parseDouble(propina);
			this.recargos = Double.parseDouble(recargos);
			this.fechaHoraFinal = fechaHoraFinal;
			this.identificacionServicio = identificacionServicio;
			this.millasRecorridas = Double.parseDouble(millasRecorridas);
			this.segundosEmpleados = Double.parseDouble(segundosEmpleados);
			this.fechaHoraInicial = fechaHoraInicial;
			this.totalServicio = Double.parseDouble(totalServicio);
		}

		catch (Exception e)		
		{
			e.printStackTrace();
		}		
	}

	//=====================================
	// Metodos
	//=====================================

	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public String darNombreCompania()
	{
		return nombreCompania;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darCensoAreaBajada()
	{
		return censoAreaBajada;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darLatitudZonaCiudadBajada()
	{
		return latitudZonaCiudadBajada;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public localizacionZonaCiudad darLocalizacionBajada()
	{
		return localizacionBajada;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darLongitudZonaCiudadBajada()
	{
		return longitudZonaCiudadBajada;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darAreaComienzo()
	{
		return areaComienzo;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darAreaFinalizacion()
	{
		return areaFinalizacion;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darPropina()
	{
		return propina;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darRecargos()
	{
		return recargos;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public Date darFechaHoraInicial()
	{
		 SimpleDateFormat formato = new SimpleDateFormat("yyyyy-MM-dd-HH:mm:ss.SSS");
	        Date fechaDate = null;
	        try {
	        	String [] fecha=fechaHoraInicial.split("T");
	            fechaDate = formato.parse(fecha[0]+"-"+fecha[1]);
	        } 
	        catch (ParseException ex) 
	        {
	            System.out.println(ex);
	        }
	        return fechaDate;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public Date darFechaHoraFinal()
	{
		SimpleDateFormat formato = new SimpleDateFormat("yyyyy-MM-dd-HH:mm:ss.SSS");
        Date fechaDate = null;
        try {
        	String [] fecha=fechaHoraFinal.split("T");
            fechaDate = formato.parse(fecha[0]+"-"+fecha[1]);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public String darIdentificacionServicio()
	{
		return identificacionServicio;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public String darIdentificacionTaxi()
	{
		return identificacionTaxi;
	}

	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darSegundosEmpleados()
	{
		return segundosEmpleados;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darTotalServicio()
	{
		return totalServicio;
	}
	
	/**
	 * Permite devolver el elemento que enuncia la signatura del metodo.
	 * @return Un elemento del registro.
	 */
	
	public double darMillasRcorridas()		
	{
		return millasRecorridas;
	}

	/**
	 * Permite representa la localizacion de la zona de la ciudad donde se bajo o se subio el pasajero.
	 * Claves a representar en el archivo JSON: dropoff_centroid_location, pickup_centroid_location
	 * @author Geovanny Gonzalez
	 */	
	
	public class localizacionZonaCiudad
	{		
		//=====================================
		// Atributos
		//=====================================
		
		/**
		 * Permite representar a que se refieren las coordenadas en la localizacion
		 * Clave a representar en el archivo JSON: type.
		 */
		
		@SerializedName("type")
		private String tipoDatos;
		
		/**
		 * Permite representar las coordenadas en la localizacion.
		 * Clave a representar en el archivo JSON: coordinates.
		 */
		
		@SerializedName("coordinates")
		private double [] coordenadasLocalizacion;
		
		//=====================================
		//Constructor
		//=====================================
		
		/**
		 * Permite generar un objeto para guardar la clave JSON asociada.
		 * Clave a representar en el archivo JSON: dropoff_centroid_location
		 * @param tipoDatos Es la forma como se interpretaran las coordenadas
		 * @param coordenadasLocalizacion Son las coordenadas de la zona de la ciudad donde finalizo o comenzo el servicio.
		 */
		
		public localizacionZonaCiudad (String tipoDatos, double[] coordenadasLocalizacion)
		{
			this.tipoDatos = tipoDatos;
			this.coordenadasLocalizacion = coordenadasLocalizacion;
		}
		
		/**
		 * Permite devolver las coordenadas. El indice 0 corresponde a la coordena 0 del JSON y el indice 1 a la coordenada 1.
		 * @return Coordenadas de la zona donde finalizo el servicio.
		 */
		
		public double[] darCoordendas()
		{
			return coordenadasLocalizacion;
		}
		
		/**
		 * Permite retornar la informacion sobre como se interpretan las coordenadas.
		 * @return Tipo de datos de localizacion.
		 */
		
		public String darTipoDatos()
		{
			return tipoDatos;
		}			
	}

	/**
	 * Permite realizar labores de comparacion entre registro. Se compara por millas recorridas.
	 * @return cero en caso de que ambos objetos sean iguales. En caso contrario compara con respecto a las fechas. 
	 */
	
	@Override
	public int compareTo(Servicio elementoEntrante) {
		
		if (this.equals(elementoEntrante))
			return 0;
		
		return ((Double) this.darMillasRcorridas()).compareTo(((Double)elementoEntrante.darMillasRcorridas())) ;
	}	
}
