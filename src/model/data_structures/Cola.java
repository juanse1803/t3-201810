package model.data_structures;

/**
 * Clase que representa una estructura de datos Cola
 * @author Geovanny Gonzalez.
 *
 * @param <T> Puede ser cualquier clase
 */

public class Cola <T> implements IQueue<T>
{

	// ===========================
	// Atributos
	// ===========================

	/**
	 * Cantidad de elementos.
	 */

	private int longitud;

	/**
	 * Referencia al primer nodo agregado en la cola.
	 */

	private Node<T> primerAgregado;

	/**
	 * Referencia al ultimo nodo agregado en la cola.
	 */

	private Node<T> ultimoAgregado;

	// ===========================
	// Constructor
	// ===========================

	/**
	 * Permite crear una nueva pila
	 */

	public Cola()
	{
		primerAgregado = null;
		ultimoAgregado = null;
		longitud = 0;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Permite conocer el estado de la pila.
	 * @return Un valor booleano si la pila esta vacia.
	 */

	public boolean estaVacio()
	{
		return primerAgregado == null;
	}

	/**
	 * Permite agregar un nuevo elemento a la cola
	 * @param elemento El elemento a ser agregado en el nodo de la cola.
	 */

	public void enqueue(T elemento)
	{
		Node<T> nuevoNodo = new Node<T>(elemento);	

		if (estaVacio()) // Agregar el primer elemento a la cola
		{
			ultimoAgregado = nuevoNodo;
			primerAgregado = ultimoAgregado;
		}

		else // Ya hay elementos en la cola. Se agregan al final
		{			
			ultimoAgregado.cambiarSiguiente(nuevoNodo);
			ultimoAgregado = nuevoNodo;
		}

		longitud++;
		verificarInvariante();
	}

	/**
	 * Permite extraer el elemento en el tope de la cola.	 
	 * @return El elemento al tope de la pila. Retorna null si no hay elementos.
	 */

	public T dequeue()
	{		
		if (!estaVacio()) // Si hay elementos en la lista para borrar.
		{
			T elemento = primerAgregado.darElemento();
			primerAgregado = primerAgregado.darSiguiente();

			if (estaVacio()) // Si se ha eliminado el unico elemento en la cola
			{
				ultimoAgregado = null;
			}

			longitud--;
			return elemento;	
		}

		return null;
	}

	/**
	 * Permite devolver el numero de elementos en la Cola
	 * @return Numero de elementos.
	 */

	public int size()
	{
		return longitud;
	}

	/**
	 * Permite verificar las invariantes de la clase
	 * inv:
	 * longitud >= 0
	 */

	private void verificarInvariante()
	{
		assert longitud >= 0 : "Longitud invalida";
		assert ultimoAgregado.darSiguiente() == null: "Hay elementos despues del ultimo";
	}	
}
