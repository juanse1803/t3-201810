package model.data_structures;

/**
 * Clase que representa una estructura de datos Pila
 * @author Geovanny Gonzalez.
 *
 * @param <T> Puede ser cualquier clase
 */

public class Pila <T> implements IStack<T> {

	// ===========================
	// Atributos
	// ===========================

	/**
	 * Cantidad de elementos.
	 */

	private int longitud;

	/**
	 * Referencia al primer nodo en la pila.
	 */

	private Node<T> primero;

	// ===========================
	// Constructor
	// ===========================

	/**
	 * Permite crear una nueva pila
	 */

	public Pila()
	{
		primero = null;
		longitud = 0;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Permite conocer el estado de la pila.
	 * @return Un valor booleano si la pila esta vacia.
	 */

	public boolean estaVacio()
	{
		return primero == null;
	}

	/**
	 * Permite agregar un nuevo elemento al tope de la pila
	 * @param elemento El elemento a ser agregado en el nodo de la pila.
	 */

	public void push(T elemento)
	{
		Node <T> nuevoNodo = new Node<T>(elemento);
		nuevoNodo.cambiarSiguiente(primero);
		primero = nuevoNodo;
		longitud++;
	}

	/**
	 * Permite extraer el elemento en el tope de la pila.
	 * @return El elemento al tope de la pila. Si no hay elementos retorna null
	 */

	public T pop()
	{		
		if (!estaVacio()) // Si hay elementos en la Pila
		{
			T elementoTope = primero.darElemento();
			primero = primero.darSiguiente();
			longitud--;
			return elementoTope;
		}

		return null;  // Si no hay elementos
	}

	/**
	 * Permite devolver el numero de elementos en la lista 
	 * @return Numero de elementos en la lista
	 */

	public int size()	
	{
		return longitud;
	}

	/**
	 * Permite verificar las invariantes de la clase
	 * inv:
	 * longitud >= 0
	 */

	private void verificarInvariante()
	{
		assert longitud >= 0 : "Longitud invalida";		
	}	
}
