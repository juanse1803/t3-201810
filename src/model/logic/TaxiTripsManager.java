package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Pila;
import model.vo.Taxi;
import model.vo.Servicio;

public class TaxiTripsManager implements ITaxiTripsManager {

	// ===========================
	// Atributos
	// ===========================

	/**
	 * Es el arreglo de salida con los servicios (registros del archivo JSON) 
	 */

	private Servicio[] registroServicios;

	/**
	 * Es la pila en la cual se van a guardar los servicios del taxi.
	 */

	private Pila<Servicio> pilaServiciosTaxi;

	/**
	 * Es una cola en la cual se van a guardar los servicios del taxi.
	 */

	private Cola<Servicio> colaServiciosTaxi;

	// ===========================
	// Constructor
	// ===========================

	/**
	 * Permite crear una interfaz de control para realizar operaciones en el aplicativo.
	 */

	public TaxiTripsManager()
	{
		registroServicios = null;
		pilaServiciosTaxi = new Pila<Servicio>();
		colaServiciosTaxi = new Cola<Servicio>();
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Permite cargar los servicios del archivo JSON ademas de cargar los servicios de un taxi especifico
	 * a una Pila y a una cola
	 * @param serviceFile: Es la ruta donde esta el archivo JSON
	 * @param taxiId: Es la identificacion del taxi al cual se le van a guardar los servicios.
	 */
	
	public void loadServices (String serviceFile, String taxiId) {
		final File archivoJson = new File(serviceFile);			
		final Gson elGson = new Gson();		
		final Type tipoServicio = new TypeToken<Servicio[]>() {}.getType();		

		try (FileReader tubo = new FileReader(archivoJson); BufferedReader lector = new BufferedReader(tubo))
		{
			registroServicios = elGson.fromJson(lector, tipoServicio);			
		}

		catch (IOException error)
		{
			error.printStackTrace();
		}

		if (!registroServicios[0].darIdentificacionTaxi().isEmpty())
		{
			System.out.println("Archivo cargado con exito !");
			System.out.println("Numero de elementos cargados: " + registroServicios.length);
			System.out.println("Cargando los archivos del taxi: " + taxiId +  "en la estructura Pila y Cola");
			
			for (Servicio actual: registroServicios) // Ojo si no sirve puede ser por el for-each que no es util en arrays.
			{
				if (actual.darIdentificacionTaxi().equalsIgnoreCase(taxiId))
				{
					pilaServiciosTaxi.push(actual);
					colaServiciosTaxi.enqueue(actual);
				}
			}
			
			System.out.println("Se han cargado en las estructuras de datos un total de: " + pilaServiciosTaxi.size() + " servicios.");
		}
		
		else
			System.out.println("Error cargando los archivos");
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		
		int correctos=1,incorrectos=0;
		if(!pilaServiciosTaxi.estaVacio()) {
		Date ant =pilaServiciosTaxi.pop().darFechaHoraInicial();
		for(int i=0;i<=pilaServiciosTaxi.size();i=1) {
		Date act=ant;
		ant=pilaServiciosTaxi.pop().darFechaHoraInicial();
			if((act.compareTo(ant)>=0))
				{
			correctos++;
				}
		else {
			incorrectos++;
		ant=act;
		}
		}
		}
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		resultado[0]=correctos;
		resultado[1]=incorrectos;
		return resultado;
	}

	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		
		
		int correctos=1,incorrectos=0;
		if(!colaServiciosTaxi.estaVacio()) {
		Date ant =colaServiciosTaxi.dequeue().darFechaHoraInicial();
		for(int i=0;i<=colaServiciosTaxi.size();i=1) {
		
			Date act=ant;
		ant=colaServiciosTaxi.dequeue().darFechaHoraInicial();
			if((act.compareTo(ant))<=0)
				{
			correctos++;
			
				}
		else {
			incorrectos++;
			ant=act;
		}
		}
		}
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		resultado[0]=correctos;
		resultado[1]=incorrectos;
		return resultado;
	}


}
