package src;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import model.data_structures.Pila;

/**
 * Permite realizar pruebas a la estructura de la Pila 
 * @author Geovanny Gonzalez
 */

public class TestPila {
	
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
	/**
	 * Permite crear una pila de String con los cuales se van a realizar las pruebas.
	 */
	
	private Pila <String> pilaConNombres;
	
	/**
	 * Permite crear un escenario con una pila vacia.
	 */
	
	// -----------------------------------------------------------------
    // Escenarios
    // -----------------------------------------------------------------
	
	@Before
	public void PilaVacia() 
	{
		pilaConNombres = new Pila <String> ();
	}
	
	/**
	 * Permite crear un escenario de una pila con elementos.
	 */
	
	public void PilaConElementos()
	{		
		pilaConNombres.push("Rodriguez");
		pilaConNombres.push("Gonzalez");
		pilaConNombres.push("Andres");
		pilaConNombres.push("Geovanny");
	}
	
	// -----------------------------------------------------------------
    // Metodos a Probar
    // -----------------------------------------------------------------
	
	/**
	 * push(T)
	 * pop()
	 */
	
	/**
	 * Permite probar el metodo push de la estructura de datos Pila.
	 */
	
	@Test	
	public void push()
	{		
		//Caso Unico: Agregar elementos en el tope de la pila.
		
		pilaConNombres.push("Gonzalez");
		pilaConNombres.push("Geovanny");		
		assertEquals("La longitud deberia ser 2", 2, pilaConNombres.size());
		
		String elementos = pilaConNombres.pop() + pilaConNombres.pop();
		assertEquals("El orden de los elementos al agregar no es correcto", elementos, "GeovannyGonzalez");		
	}
	
	/**
	 * Permite probar el metodo de pop de la estructura de datos Pila 
	 */
	
	@Test
	public void pop()
	{
		
		// Caso Unico: Se comprueba el tama�o de la lista cada vez que se sacan elementos.
		// Al final se mira que el orden en que se sacaron sea correcto.
		
		PilaConElementos();
		String elementos = "";
		int tamanioInicial = pilaConNombres.size();
		
		while (!pilaConNombres.estaVacio())
		{
			elementos += pilaConNombres.pop();
			tamanioInicial--;
			assertEquals("Los tama�os difieren", tamanioInicial, pilaConNombres.size());
		}
		
		assertEquals ("La lista deberia estar vacia", 0, pilaConNombres.size());
		assertEquals ("El orden de la cadena no es correcto", elementos, "GeovannyAndresGonzalezRodriguez");		
	}	
}
