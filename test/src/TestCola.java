package src;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.data_structures.Cola;

/**
 * Prueba la estructura Cola y sus metodos
 * @author Geovanny Gonzalez
 */

public class TestCola {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Permite crear una cola de String con los cuales se van a realizar las pruebas.
	 */

	private Cola <String> colaDeLetras;

	// -----------------------------------------------------------------
	// Escenarios
	// -----------------------------------------------------------------

	/**
	 * Permite crear un escenario con una cola vacia.
	 */

	@Before
	public void ColaVacia() 
	{
		colaDeLetras = new Cola <String>();
	}

	// -----------------------------------------------------------------
	// Metodos a Probar
	// -----------------------------------------------------------------

	/**
	 *  estaVacio()
	 *  enqueue(T)
	 *  dequeue()	
	 */

	@Test
	public void estaVacio()
	{
		// Caso #1: La lista se encuentra vacia

		assertTrue("La lista deberia estar vacia", colaDeLetras.estaVacio());

		// Caso #2: La lista posee elementos.

		colaDeLetras.enqueue("Hola");		
		assertFalse ("La lista deberia tener un elemento al menos", colaDeLetras.estaVacio());		
	}

	@Test
	public void enqueue_dequeue()
	{
		// Caso Unico: Agregar elementos y que se encuentren en orden.
		colaDeLetras.enqueue("A");
		assertFalse ("La lista deberia tener un elemento al menos", colaDeLetras.estaVacio());
		colaDeLetras.enqueue("B");
		colaDeLetras.enqueue("C");
		assertEquals("La longitud de la cola deberia ser 3", 3, colaDeLetras.size());

		// Sacarlos en orden
		String elementos = "";
		int tamanioInicial = colaDeLetras.size();

		while (!colaDeLetras.estaVacio())
		{
			elementos += colaDeLetras.dequeue();
			tamanioInicial--;
			assertEquals("Los valores deberian corresponder", tamanioInicial, colaDeLetras.size());
		}

		assertEquals("El orden no es el esperado", elementos, "ABC");	
	}	
}
